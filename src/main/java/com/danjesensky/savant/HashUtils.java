package com.danjesensky.savant;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

/**
 * A utility that implements some common practices for hashing. Often times, these will be used for authentication.
 */
public final class HashUtils {
    private static final String PBKDF2_ALGORITHM_NAME = "PBKDF2WithHmacSHA512";
    private static final String DEFAULT_HASH_NAME = "HmacSHA512";
    private static final int DEFAULT_KEY_BYTES = 64;
    private static final int DEFAULT_PBKDF2_ITERATIONS = 32_000;

    private static final SecureRandom random;

    static {
        random = new SecureRandom();
    }

    /**
     * Compares two strings, slowly.<br>
     * <br>
     * This function is intentionally slow in order to prevent side-channel timing attacks. Short-circuiting benefits
     * an attacker with patience.
     *
     * @param s1 The first string to compare.
     * @param s2 THe second string to compare.
     *
     * @return Whether or not the strings provided matched.
     */
    public static boolean verify(String s1, String s2){
        if(s1 == null || s2 == null || s1.length() != s2.length()){
            return false;
        }

        //intentionally slow and inefficient (i.e. doesn't short-circuit) to prevent timing attacks
        boolean match = true;
        for(int i = 0; i < s1.length(); i++){
            if(s1.charAt(i) != s2.charAt(i)){
                match = false;
            }
        }

        return match;
    }

    /**
     * Hash a plain text input, using a key derived by PBKDF2 from the salt and password.
     *
     * @param password The password to apply PBKDF2 to. This is not the plaintext. This array should generally be zeroed after calling this method.
     * @param salt The salt to use when hashing the plaintext.
     * @param rounds The number of rounds of PBKDF2 to apply to the password and salt to arrive at a key.
     * @param keyBytes The number of bytes to generate for the key with PBKDF2.
     * @param plaintext The plaintext to hash.
     *
     * @return The hashed text, as a base16 string.
     *
     * @throws NoSuchAlgorithmException If the JVM does not support the SHA2-512 algorithm, which should not occur as long as you are staying up-to-date with the JDK version.
     * @throws UnsupportedEncodingException If the JVM does not support the UTF-8 encoding, which as above, should never happen.
     * @throws InvalidKeySpecException If the JVM does not support PBKDF2. As above, this should not happen unless you are running probably Java 4-5.
     * @throws InvalidKeyException As above.
     */
    public static String hash(char[] password, byte[] salt, int rounds, int keyBytes, String plaintext) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeySpecException, InvalidKeyException {
        PBEKeySpec spec = new PBEKeySpec(password, salt, rounds, keyBytes);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM_NAME);
        SecretKey key = skf.generateSecret(spec);

        Mac mac = Mac.getInstance(DEFAULT_HASH_NAME);
        mac.init(key);

        byte[] pt = plaintext.getBytes("UTF-8");
        byte[] digest = mac.doFinal(pt);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < digest.length; i++) {
            sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    /**
     * Hash a plain text password with SHA2-512, using a key derived by PBKDF2 from the salt and password.
     *
     * @param plaintext The text to hash.
     * @param password The password to use when hashing.
     * @param salt The salt to use when hashing the password.
     * @return The hashed password. 512/8*2 = 128 characters long.
     *
     * @throws NoSuchAlgorithmException If the JVM does not support the SHA2-512 algorithm, which should not occur as long as you are staying up-to-date with the JDK version.
     * @throws UnsupportedEncodingException If the JVM does not support the UTF-8 encoding, which as above, should never happen.
     * @throws InvalidKeySpecException If the JVM does not support PBKDF2. As above, this should not happen unless you are running probably Java 4-5.
     * @throws InvalidKeyException As above.
     */
    public static String hash(char[] password, byte[] salt, String plaintext) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeySpecException, InvalidKeyException{
        return hash(password, salt, DEFAULT_PBKDF2_ITERATIONS, DEFAULT_KEY_BYTES, plaintext);
    }

    /**
     * Generates a salt using Java's built-in CSPRNG (cryptographically-secure pseudo-random number generator).
     *
     * @return A 64-byte salt for hashing.
     */
    public static byte[] generateSalt(){
        return generateSalt(64);
    }

    /**
     * Generates a salt using Java's built-in CSPRNG (cryptographically-secure pseudo-random number generator).
     *
     * @param size The number of bytes to generate.
     *
     * @return A 64-byte salt for hashing.
     */
    public static byte[] generateSalt(int size){
        byte[] salt = new byte[size];
        random.nextBytes(salt);
        return salt;
    }
}
