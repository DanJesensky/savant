package com.danjesensky.savant;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public final class CollectionUtils {
    public static <T> T[] listToArray(List<T> list){
        return list.toArray((T[])new Object[list.size()]);
    }

    public static <T> String stringifyArray(T[] array){
        StringBuilder sb = new StringBuilder();
        for(T t: array) {
            sb.append(t.toString());
            sb.append(", ");
        }
        return sb.substring(0, sb.length()-2);
    }

    public static <T> String stringifyCollection(Collection<T> list){
        //this is not just a call to stringifyArray(listToArray(list)) because of performance concerns
        StringBuilder sb = new StringBuilder();
        for(T t: list) {
            sb.append(t.toString());
            sb.append(", ");
        }
        return sb.substring(0, sb.length()-2);
    }

    public static byte[] base16ToBytes(String base16){
        byte[] result = new byte[base16.length()/2];
        for(int i = 0, r = 0; i < base16.length(); i+=2, r++) {
            result[r] = (byte)(Short.parseShort(base16.substring(i, i+2), 16) - 0x100);
        }
        return result;
    }

    public static String bytesToBase16(byte[] bytes){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString().toUpperCase();
    }
}
