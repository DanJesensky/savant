package com.danjesensky.savant.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    Hash.class,
    Collections.class
})
public class TestSuite {
}
