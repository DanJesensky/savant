package com.danjesensky.savant.tests;

import com.danjesensky.savant.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class Collections {
    @Test
    public void base16(){
        byte[] b = {0, 1, 127, -127, -128, 100, -100};
        String s = CollectionUtils.bytesToBase16(b);
        byte[] r = CollectionUtils.base16ToBytes(s);
        System.out.println(s);

        Assert.assertTrue(s.compareTo(CollectionUtils.bytesToBase16(r)) == 0);

        Assert.assertEquals(b.length, r.length);
        for(int i = 0; i < b.length; i++){
            Assert.assertEquals(b[i], r[i]);
        }
    }
}
