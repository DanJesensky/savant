package com.danjesensky.savant.tests;

import com.danjesensky.savant.HashUtils;
import org.junit.Assert;
import org.junit.Test;

public class Hash {
    @Test
    public void compareSamePlaintextHashes() throws Exception{
        byte[] salt1 = HashUtils.generateSalt();
        byte[] salt2 = HashUtils.generateSalt();
        String plaintext = "something secret";
        char[] password = "an easily guessed key".toCharArray();

        String h1 = HashUtils.hash(password, salt1, plaintext);
        String h2 = HashUtils.hash(password, salt2, plaintext);
        String h3 = HashUtils.hash(password, salt1, plaintext);

        Assert.assertNotEquals(h1, h2);
        Assert.assertEquals(h1, h3);
    }

    @Test
    public void compareDifferentPlaintextHashes() throws Exception{
        byte[] salt = HashUtils.generateSalt();
        String plaintext1 = "something secret";
        String plaintext2 = "something different";
        char[] password = "an easily guessed key".toCharArray();

        String h1 = HashUtils.hash(password, salt, plaintext1);
        String h2 = HashUtils.hash(password, salt, plaintext2);
        String h3 = HashUtils.hash(password, salt, plaintext1);

        Assert.assertEquals(h1, h3);
        Assert.assertNotEquals(h1, h2);
    }
}
